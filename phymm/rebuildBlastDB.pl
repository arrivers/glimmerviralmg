#!/usr/bin/perl

use strict;

$| = 1;

#######################################################################################################
# 
# Rebuild the BLAST DB.
# 
#######################################################################################################

print "\nAre you sure you want to rebuild the BLAST database (Y/N): ";

my $response = <STDIN>;

chomp $response;

while ( length($response) != 1 or $response !~ /[yYnN]/ ) {
   
   print "Please enter Y or N: ";

   $response = <STDIN>;

   chomp $response;
}

my $logDir = '.logs/setup';

system("mkdir -p $logDir");

if ( $response =~ /[yY]/ ) {
   
   print "Rebuilding BLAST database...";

   &rebuildBlastDB();

   print "done.  Logs can be found at \"$logDir/rebuildBlastDB_*.txt\".\n\n";

} else {
   
   print "\nOkay; exiting without rebuilding BLAST database.\n\n";
}

# Rebuild the local BLAST database from the RefSeq FASTA files (plus any genome files added by users as well).

sub rebuildBlastDB {
   
   my $locateBlastBinary = `which makeblastdb 2>&1`;
   
   if ( $locateBlastBinary =~ /no\s+makeblastdb\s+in/ or $locateBlastBinary =~ /command\s+not\s+found/i ) {
      
      die("FATAL: You must have a local copy of the BLAST software installed and accessible via your \"PATH\" environment variable.  Please see the README file for details.\n\n");

   } else {
      
      # Standalone BLAST is installed.

      # Concatenate all genome files and build a local BLAST DB from them.

      # First, grab a list of relative paths to all the RefSeq genomic FASTA files.

      my @fastaFiles = ();

      opendir DOT, '.genomeData' or die("FATAL: Can't open .genomeData for scanning.\n");

      my @subs = grep { !/^\./ } sort { $a cmp $b } readdir DOT;

      closedir DOT;

      foreach my $sub ( @subs ) {
	 
	 if ( -d ".genomeData/$sub" ) {
	    
	    opendir DOT, ".genomeData/$sub" or die("FATAL: Can't open .genomeData/$sub for scanning.\n");

	    my @files = sort { $a cmp $b } grep { /\.fna$/ } readdir DOT;

	    closedir DOT;

	    foreach my $file ( @files ) {
	       
	       push @fastaFiles, ".genomeData/$sub/$file";
	    }
	 }
      }
	 
      # Next, grab a list of relative paths to any user-added FASTA files.
	 
      my $userDir = '.genomeData/.userAdded';
	 
      if ( -e $userDir ) {
	 
	 opendir DOT, $userDir or die("FATAL: Can't open $userDir for scanning.\n");

	 @subs = grep { !/^\./ } sort { $a cmp $b } readdir DOT;

	 closedir DOT;

	 foreach my $sub ( @subs ) {
	    
	    if ( -d "$userDir/$sub" ) {
	       
	       opendir DOT, "$userDir/$sub" or die("FATAL: Can't open $userDir/$sub for scanning.\n");

	       my @files = sort { $a cmp $b } grep { /\.fna$/ } readdir DOT;

	       closedir DOT;

	       foreach my $file ( @files ) {
		  
		  push @fastaFiles, "$userDir/$sub/$file";
	       }
	    }
	 }
      }

      # Concatenate all the FASTA files together for database creation.

      my $command = 'cat ';

      my $wrapped = 0;

      my $fileIndex = -1;

      foreach my $file ( @fastaFiles ) {
	 
         $fileIndex++;

	 $command .= "$file \\\n";

	 if ( ( $fileIndex + 1 ) % 10 == 0 and $fileIndex != $#fastaFiles ) {
               
            if ( not $wrapped ) {
                  
               $wrapped = 1;

               $command .= "> blastDB_data.txt\n";

            } else {
                  
               $command .= ">> blastDB_data.txt\n";
            }

            $command .= 'cat ';
	 }
      }
	 
      if ( not $wrapped ) {
	 
	 $command .= "> blastDB_data.txt\n";

      } else {
	 
	 $command .= ">> blastDB_data.txt\n";
      }
	 
      # There's a problem with using system() for a command this long, apparently, so we use a hack.

      my $shLoc = `which sh`;

      chomp $shLoc;

      my $outFile = "command_temp.sh";

      open OUT, ">$outFile" or die("FATAL: Can't open $outFile for writing.\n");

      print OUT "#!$shLoc\n\n$command";

      close OUT;

      system("chmod 775 command_temp.sh");

      system("./command_temp.sh");

      system("rm command_temp.sh");

      # Create the local database copy.

      system("makeblastdb -in blastDB_data.txt -title phymm_BLAST_DB -out phymm_BLAST_DB -dbtype nucl > $logDir/rebuildBlastDB_out.txt 2> $logDir/rebuildBlastDB_err.txt");

      system("mv phymm_BLAST_DB.* .blastData");

      system("rm blastDB_data.txt");

   } # end if ( local copy of BLAST binary is installed )
}

