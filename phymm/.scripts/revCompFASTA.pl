#!/usr/bin/perl

my $inFile = shift;

die("Usage: $0 <inFile>\n") if not $inFile;

open IN, "<$inFile" or die("Can't open $inFile for reading.\n");

my $seq = '';

my $header = '';

my $first = 1;

my $outFile = $inFile;

if ( $inFile =~ /(\.[^\.]+)$/ ) {
   
   my $extension = $1;
   
   $outFile =~ s/$extension$/\.revComp$extension/;

} else {
   
   die("Couldn't identify file extension for $inFile.\n");
}

open OUT, ">$outFile" or die("Can't open $outFile for writing.\n");

while ( my $line = <IN> ) {
   
   if ( $line =~ /^>/ and $first == 1 ) {
      
      $header = $line;
      
      $first = 0;

   } elsif ( $line =~ /^>/ ) {
      
      my $printSeq = &revComp($seq);

      print OUT $header;

      print OUT &fastaFy($printSeq);

      $header = $line;

      $seq = '';

   } elsif ( $line =~ /(\S+)/ ) {
      
      $seq .= $1;
   }
}

close IN;

print OUT $header;

my $printSeq = &revComp($seq);

print OUT &fastaFy($printSeq);

close OUT;


sub revComp {

   my $seq = shift;

   $seq =~ s/A/Q/g;

   $seq =~ s/T/A/g;

   $seq =~ s/Q/T/g;


   $seq =~ s/G/Q/g;

   $seq =~ s/C/G/g;

   $seq =~ s/Q/C/g;


   $seq = reverse($seq);

   return $seq;
}

sub fastaFy {
   
   my $seq = shift;

   my $index = 0;

   my $result = '';

   while ( length($seq) - $index > 60 ) {
      
      $result .= substr($seq, $index, 60) . "\n";

      $index += 60;
   }
   
   if ( $index < length($seq) ) {
      
      $result .= substr($seq, $index) . "\n";
   }

   return $result;
}





