#!/usr/bin/perl

use strict;

$| = 1;

my $rootDir = shift;

my $outFile = shift;

my $errFile = shift;

close STDERR;

open STDERR, '>>', $errFile or die("$0: FATAL: Couldn't internally redirect STDERR to \"$errFile\".\n");

die("Usage: $0 <root directory for search> <output log> <error log>\n") if not $errFile;

open OUT, ">>$outFile" or die("$0: FATAL: Can't open $outFile for appending.\n");

opendir IN, $rootDir or die("$0: FATAL: Couldn't open $rootDir for scanning.\n");

my @dirs = grep { !/^\./ } sort { $a cmp $b } readdir IN;

closedir IN;

my $doneCount = 0;

# First, count the number of ICMs to be built so we can give the user an idea about our progress as we build new ones.

my $toBuildCount = 0;

foreach my $dir ( @dirs ) {
   
   if ( -d "$rootDir/$dir" ) {
      
      &countUnbuiltICMs("$rootDir/$dir");
   }
}

if ( $toBuildCount > 0 ) {
   
   # Now build any unbuilt ICMs.
   
   my $plural = '';

   if ( $toBuildCount > 1 ) {
      
      $plural = 's';
   }

   my $date = `date`;

   chomp $date;

   print "[$date] Beginning construction of $toBuildCount IMM$plural (this will take a while if you haven't yet done it)...\n\n";

   print OUT "[$date] Beginning construction of $toBuildCount IMM$plural...\n\n";

   foreach my $dir ( @dirs ) {
      
      if ( -d "$rootDir/$dir" ) {
	 
	 &makeICMsForSubdir("$rootDir/$dir");
      }
   }

   $date = `date`;

   chomp $date;

   print "\n[$date] ...done.  Built $doneCount IMM${plural}.";

   print OUT "\n[$date] ...done.  Built $doneCount IMM${plural}.";
}


sub countUnbuiltICMs {
   
   my $dir = shift;

   opendir DOT, "$dir" or die("$0: FATAL: countUnbuiltICMs(): couldn't open $dir for scanning.\n");

   my @files = readdir DOT;

   closedir DOT;

   foreach my $file ( @files ) {
      
      if ( $file =~ /(.+)\.\d+bpGenomicTrainingSeqs.txt$/ ) {
	 
	 my $prefix = $1;

	 if ( not -e "$dir/$prefix\.icm" ) {
	    
	    $toBuildCount++;
	 }
      }
   }
}

sub makeICMsForSubdir {
   
   my $root = shift;

   opendir DOT, "$root" or die("$0: FATAL: makeICMsForSubdir(): Can't open $root for scanning.\n");

   my @subs = sort { $a cmp $b } readdir DOT;

   closedir DOT;
   
   my $foundTrain = 0;

   foreach my $sub ( @subs ) {
      
      if ( $sub =~ /bpGenomicTrainingSeqs\.txt/ ) {
	 
	 $foundTrain = 1;
      }
   }

   if ( $foundTrain ) {
      
      # Found a training file.  Generate genomic ICMs from all training sequence files in this directory.
      
      foreach my $file ( @subs ) {
	 
	 if ( $file =~ /(.+)\.\d+bpGenomicTrainingSeqs\.txt/ ) {
	    
	    my $prefix = $1;
	    
	    if ( not -e "$root/$prefix\.icm" ) {
	       
	       &generateICM($root, $prefix, $file);
	    }
	 }
      }
   }
}


sub generateICM {
   
   my $dir = shift;

   my $prefix = shift;
   
   my $trainFile = shift;

   my $command = ".scripts/.icmCode/bin/build-icm -d 10 -w 12 -p 1 $dir/$prefix.icm < $dir/$trainFile";
      
   system("$command\n");

   my $icmSizeCheck = -s "$dir/$prefix.icm";

   if ( $icmSizeCheck == 0 ) {
      
      # Try once more.  Every once in a long while the ICM construction
      # call fails mysteriously, and will succeed if re-run.

      system($command);

      $icmSizeCheck = -s "$dir/$prefix.icm";

      if ( $icmSizeCheck == 0 ) {
         
         die("$0: generateICM(): FATAL: Could not construct nonzero-length ICM \"$dir/$prefix.icm\".\n");
      }
   }

   $doneCount++;

   my $date = `date`;

   chomp $date;

   if ( $doneCount % 10 == 0 ) {

      print OUT "[$date]    ...finished $doneCount/$toBuildCount...\n";
   }

   if ( $doneCount % 100 == 0 ) {
      
      print "[$date]    ...finished $doneCount/$toBuildCount...\n";
   }
}

